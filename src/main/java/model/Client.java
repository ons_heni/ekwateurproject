package model;

import Exceptions.ReferenceException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Client {

    private String ReferenceClient;
    private double ConsommationElec;
    private double ConsommationGaz;

    public Client(String referenceClient) throws ReferenceException {
        validateRef(referenceClient);
        ReferenceClient = referenceClient;
    }

    private void validateRef(String ref) throws ReferenceException {
        Pattern p = Pattern.compile("^EKW[0-9]{8}");
        Matcher m = p.matcher(ref);
        if (!m.matches()) throw new ReferenceException("La reference client n'est pas valide");
    }

    public String getReferenceClient() {
        return ReferenceClient;
    }

    public double getConsommationElec() {
        return ConsommationElec;
    }

    public void setConsommationElec(double consommationElec) {
        ConsommationElec = consommationElec;
    }

    public double getConsommationGaz() {
        return ConsommationGaz;
    }

    public void setConsommationGaz(double consommationGaz) {
        ConsommationGaz = consommationGaz;
    }

    public abstract double calculConsommationElec();

    public abstract double calculConsommationGaz();

    public double calculConsommationGlobal() {
        return calculConsommationElec() + calculConsommationGaz();
    }
}

