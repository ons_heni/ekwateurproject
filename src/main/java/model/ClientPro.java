package model;

import Exceptions.ReferenceException;

import static model.PrixKW.*;

public class ClientPro extends Client{


    public ClientPro(String reference, double ca) throws ReferenceException {
        super(reference);
        this.ca = ca;
    }

    private String siret;
    private String raisonSociale;
    private double ca;

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public double getCa() {
        return ca;
    }

    public void setCa(double ca) {
        this.ca = ca;
    }

    public double getKwElecPrice() {
        return  ca > caRef ? kwElecHighCa : kwElecLowCa;
    }

    public double getKwGazPrice() {
        return  ca > caRef ? kwGazHighCa  : kwGazLowCa;
    }

    @Override
    public double calculConsommationElec(){
        return getKwElecPrice() * getConsommationElec();
    }

    public double calculConsommationGaz(){
        return getKwGazPrice() * getConsommationGaz();
    }
}
