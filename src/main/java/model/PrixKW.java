package model;

/**
 * Prices for a kw of consumption
 */
interface PrixKW {
    double kwElec = 0.121;
    double kwGaz = 0.115;
    double kwElecHighCa = 0.114;
    double kwGazHighCa = 0.111;
    double kwElecLowCa = 0.118;
    double kwGazLowCa = 0.113;

    double caRef = 1000000;
}