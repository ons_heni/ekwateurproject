package model;

import Exceptions.ReferenceException;

import static model.PrixKW.*;

public class ClientParticulier extends Client {


    public ClientParticulier(String reference) throws ReferenceException {
        super(reference);
    }

    private String civilite;
    private String nom;
    private String prenom;

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public double calculConsommationElec() {
        return getConsommationElec() * kwElec;
    }

    @Override
    public double calculConsommationGaz() {
        return getConsommationGaz() * kwGaz;
    }
}
