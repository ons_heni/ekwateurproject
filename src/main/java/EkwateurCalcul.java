import Exceptions.ReferenceException;
import model.Client;
import model.ClientParticulier;
import model.ClientPro;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class EkwateurCalcul {
    public static void main(String... args) throws Exception {

        Client clientPar;
        try {
            clientPar = new ClientParticulier("EKW99999976");
            clientPar.setConsommationElec(150000);
            clientPar.setConsommationGaz(80000);
        } catch (ReferenceException e) {
            throw new Exception("Reference client non valide");
        }

        Client clientPro;
        try {
            clientPro = new ClientPro("EKW99999988", 3000000);
            clientPro.setConsommationElec(122000);
            clientPro.setConsommationGaz(150000);
        } catch (ReferenceException e) {
            throw new Exception("Reference client non valide");
        }

        Client clientPro2;
        try {
            clientPro2 = new ClientPro("EKW99968976", 60000);
            clientPro2.setConsommationElec(80000);
            clientPro2.setConsommationGaz(150000);
        } catch (ReferenceException e) {
            throw new Exception("Reference client non valide");
        }

        List<Client> clients = new ArrayList<>();
        clients.add(clientPar);
        clients.add(clientPro);
        clients.add(clientPro2);

        clients.forEach((client) -> System.out.println("le client " + client.getReferenceClient() + " a une facture de consommation globale de " +
                client.calculConsommationGlobal() + "€"));

        System.out.println("le client qui a consommé le plus est " + clients.stream().max(Comparator.comparing(c -> Double.valueOf(c.calculConsommationGlobal()))).get().getReferenceClient());
    }
}

