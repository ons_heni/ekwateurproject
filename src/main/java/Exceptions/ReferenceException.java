package Exceptions;

public class ReferenceException extends Exception {
    public ReferenceException(String message) {
        super(message);
    }
}
