import model.Client;
import model.ClientParticulier;
import model.ClientPro;
import Exceptions.ReferenceException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculTest {
    @Test
    public void calculateClientParElec() throws ReferenceException {
        Client client = new ClientParticulier("EKW99999976");
        client.setConsommationElec(150000);
        assertEquals( 18150, client.calculConsommationElec());
    }

    @Test
    public void calculateClientParGaz() throws ReferenceException {
        Client client = new ClientParticulier("EKW99999988");
        client.setConsommationGaz(80000);
        assertEquals(9200, client.calculConsommationGaz());
    }

    @Test
    public void calculateClientProElecCaOver() throws ReferenceException {
        Client client = new ClientPro("EKW99999976", 2000000);
        client.setConsommationElec(122000);
        assertEquals(13908, client.calculConsommationElec());
    }

    @Test
    public void calculateClientProGazCaOver() throws ReferenceException {
        Client client = new ClientPro("EKW99999976", 2000000);
        client.setConsommationGaz(150000);
        assertEquals(16650, client.calculConsommationGaz());
    }

    @Test
    public void calculateClientProElecCaAbove() throws ReferenceException {
        Client client = new ClientPro("EKW99999976", 800000);
        client.setConsommationElec(80000);
        assertEquals(9440, client.calculConsommationElec());
    }

    @Test
    public void calculateClientProGazCaAbove() throws ReferenceException {
        Client client = new ClientPro("EKW99999976", 800000);
        client.setConsommationGaz(150000);
        assertEquals(16950, client.calculConsommationGaz());
    }

    @Test
    public void calculateFacturationClientPar() throws ReferenceException {
        Client client = new ClientParticulier("EKW99999976");
        client.setConsommationElec(150000);
        client.setConsommationGaz(80000);
        assertEquals(27350, client.calculConsommationGlobal());
    }

    @Test
    public void calculateFacturationClientProCaOver() throws ReferenceException {
        Client client = new ClientPro("EKW99999976", 2000000);
        client.setConsommationElec(122000);
        client.setConsommationGaz(150000);
        assertEquals(30558, client.calculConsommationGlobal());
    }

    @Test
    public void calculateFacturationClientProCaAbove() throws ReferenceException {
        Client client = new ClientPro("EKW99999976", 800000);
        client.setConsommationElec(80000);
        client.setConsommationGaz(150000);
        assertEquals(26390, client.calculConsommationGlobal());
    }
}


